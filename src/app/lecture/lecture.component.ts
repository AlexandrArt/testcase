import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-lecture',
  templateUrl: './lecture.component.html',
  styleUrls: ['./lecture.component.css']
})

/**
 * Компонент для отрисовки лекции
 */
export class LectureComponent implements OnInit {
  @Input() lecturer: string;
  @Input() lecture: string;
  @Input() skill: string;
  @Input() language: string;
  constructor() { }

  ngOnInit() {
  }

}
