import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Injectable} from '@angular/core';
import {LecturesModel} from '../models/lectures.model';

@Injectable()
export class LectorsService {

  constructor(private http: HttpClient) {}

  public getJSON(): Observable<any> {
    return this.http.get<LecturesModel[]>('./assets/data.json');
  }
}
