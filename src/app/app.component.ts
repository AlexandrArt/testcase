import {Component, OnInit} from '@angular/core';
import {LectorsService} from './services/lectors.service.';
import {LecturesModel} from './models/lectures.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private cards: LecturesModel[];
  private filteredCards: LecturesModel[];

  /**
   * @param lectures - Сервис для получения лекций
   */
  constructor(private lectures: LectorsService) {}

  ngOnInit() {
    // Получение и фильтр данных
    this.lectures.getJSON().subscribe(data => {
      this.cards = data;
      this.filteredCards =  this.multiFilter(this.cards, []);
    });
  }

  /**
   * Основной фильтр по checkbox и input
   * @param array
   * @param filters
   */
  multiFilter(array, filters) {

    // Фильтр по checkbox
    let tempArray = array;
    if (filters.checkboxes ) {
      tempArray = array.filter(o =>
        Object.keys(filters.checkboxes).every(k =>
          [].concat(filters.checkboxes[k]).some(v => o[k].includes(v))));
    }

    // Фильтр по Input
    if (filters.inputs) {
      tempArray = tempArray.filter(o =>
        Object.keys(o).some(k =>
          filters.inputs[0] ? o[k].toString().toLowerCase().indexOf(filters.inputs[0].toString().toLowerCase()) !== -1 : [])
      );
    }
    return tempArray;
  }

  /**
   * EventEmitter от компонента с фильтрами. Принимает FilterConfig
   * @param event
   */
  onFilter(event) {
    this.filteredCards =  this.multiFilter(this.cards, event);
  }
}

