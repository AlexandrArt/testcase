import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LectureComponent } from './lecture/lecture.component';
import { FilterComponent } from './filter/filter.component';
import {LectorsService} from './services/lectors.service.';
import {HttpClientModule} from '@angular/common/http';
import {LecturesModel} from './models/lectures.model';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    LectureComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [LectorsService, LecturesModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
