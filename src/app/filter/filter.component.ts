import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  @Output() filterOutput = new EventEmitter();
  filterForm: FormGroup;
  checkBoxes: any = [
    {
      name: 'EN',
      type: 'language'
    },
    {
      name: 'RU',
      type: 'language'
    },
    {
      name: 'HOT',
      type: 'skill'
    },
    {
      name: 'INTERMEDIATE',
      type: 'skill'
    },
    {
      name: 'ADVANCED',
      type: 'skill'
    },
    {
      name: 'HARDCORE',
      type: 'skill'
    },
    {
      name: 'ACADEMIC',
      type: 'skill'
    }
  ];

  inputs: any = [
    {
      name: 'searchText',
      type: 'text'
    }
  ];
  constructor() {}

  ngOnInit() {
    this.createFormInputs();
    this.onChanges();
  }


  /**
   * Создание FormGroup с компонентами
   */
  createFormInputs() {
    this.filterForm = new FormGroup({
      checkboxes: this.createControl(this.checkBoxes, 'checkbox'),
      inputs: this.createControl(this.inputs, 'input')
    });
  }

  /**
   * Создание FormControl
   * @param checkboxInputs
   * @param controlType
   */
  createControl(checkboxInputs, controlType) {
    const arr = checkboxInputs.map(() => {
      if (controlType === 'checkbox') {
        return new FormControl(false);
      }
      if (controlType === 'input') {
        return new FormControl('');
      }
    });
    return new FormArray(arr);
  }

  /**
   * Отслеживание изменения формы
   */
  private onChanges() {

    this.filterForm.valueChanges.subscribe((values) => {
      const filterConfig = this.processForm(values);
      this.filterOutput.emit(filterConfig);
    });
  }

  /**
   * Перевод значения CheckBox формы в ключ - значения
   * Группировка компонентов по типу
   * @param formValues
   */
  private processForm(formValues) {
    const resultArray: any = {};
    const filterCheckboxes = formValues.checkboxes.map((item, index) => {
      if (item) {
        return this.checkBoxes[index];
      }
    }).filter(item => item);

    // Сгруппируем массив по типу и поместим в отдельную группу
    resultArray.checkboxes = filterCheckboxes.reduce((item, v) => {
      if (item[v.type]) {
        item[v.type].push(v.name);
      } else {
        item[v.type] = [v.name];
      }
      return item;
    }, {});
    resultArray.inputs = formValues.inputs;
    return resultArray;
  }

  /**
   *  Сброс фильтров
   */
  resetFilter() {
    this.filterForm.reset();
    this.filterOutput.emit([]);
  }
}
