export class LecturesModel {
  id: number;
  lecturer: string;
  lecture: string;
  skill: string;
  language: string;
}
